$('document').ready(function() {
	
	$('table #editButton').on('click',function(event){
		event.preventDefault();
		
		// in html , for every button there will be different url(href) depending on a row. main point is to get 
		// id for each clicked row and send it to controller and then controller will return object which we will use to populate 
		// fields in edit modal(form)
		
		var href = $(this).attr('href');
		
		$.get(href , function(country,status){
			$('#idEdit').val(country.id);
			$('#descriptionEdit').val(country.description);
			$('#capitalEdit').val(country.capital);
			$('#codeEdit').val(country.code);
			$('#continentEdit').val(country.continent);
			$('#nationalityEdit').val(country.nationality);
		});
		
		$('#editModal').modal();
		
	});	
	
	//this just "forwards" the href link(for controller) to the button inside modal from delete button in the row.
	$('table #deleteButton').on('click',function(event){
		event.preventDefault();
		
		var href = $(this).attr('href');
		
		$('#confirmDeleteButton').attr('href',href);
		
		
		$('#deleteModal').modal();
		
	});	
});		