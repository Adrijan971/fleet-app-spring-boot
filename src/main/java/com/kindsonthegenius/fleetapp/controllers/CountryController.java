package com.kindsonthegenius.fleetapp.controllers;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kindsonthegenius.fleetapp.models.Country;
import com.kindsonthegenius.fleetapp.services.CountryService;

@Controller
public class CountryController {

	
	@Autowired
	private CountryService countryService;
	
	@GetMapping("/countries")
	public String getCountries(Model model) {
		
		//passing list as a parameter
		model.addAttribute("countries",countryService.getCountries()); 
		
		return("country");
	}
	
	//save country
	@PostMapping("/countries/addNew")
	public String addNew(Country country) {
		countryService.save(country);
		return "redirect:/countries";
	}
	
	@RequestMapping("/countries/findById")
	@ResponseBody
	public Optional<Country> findById(int id) {
		return countryService.findByID(id);
	}
	
	
	//update country
	// put method in my opinion is not needed , but it is in tutorial ...
	@RequestMapping(value="/countries/update",method= {RequestMethod.GET,RequestMethod.PUT})
	public String update(Country country) {
		countryService.save(country);
		
		return "redirect:/countries";
	}
	
	@RequestMapping(value="/countries/delete",method= {RequestMethod.GET,RequestMethod.DELETE})
	public String delete(Integer id) {
		countryService.delete(id);
		
		return "redirect:/countries";
	}
	
	
}
