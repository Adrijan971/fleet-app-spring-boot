package com.kindsonthegenius.fleetapp.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindsonthegenius.fleetapp.models.Country;
import com.kindsonthegenius.fleetapp.repositories.CountryRepository;

@Service
public class CountryService {

	@Autowired
	private CountryRepository countryRepository;
	
	public List<Country> getCountries(){
		return countryRepository.findAll();
	}
	
	//save new country
	public void save(Country country) {
		countryRepository.save(country);
		
	}
	
	//get by id
	// optional is because if it doesnt find country , it will not crash
	public Optional<Country> findByID(int id) {
		return countryRepository.findById(id);
	}
	
	public void delete(Integer id) {
		countryRepository.deleteById(id);
	}
	
}
